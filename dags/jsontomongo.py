from typing import List, Optional, Union

#from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.utils.decorators import apply_defaults
from airflow.plugins_manager import AirflowPlugin


class JsonToMongoOperator(BaseOperator):

    template_fields = ()
    template_ext = ()
    ui_color = '#ededed'

    @apply_defaults
    def __init__(
            self,
            file_to_load: str,
            mongoserver: str,
            mongouser: str,
            mongopass: str,
            *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.file_to_load = file_to_load
        self.mongoserver = mongoserver
        self.mongouser = mongouser
        self.mongopass = mongopass

    def execute(self, context):
        #run code here to make the connection and 

class JsonToMongoPlugin(AirflowPlugin):
    name = "JsonToMongoPlugin"
    operators = [JsonToMongoOperator]
    