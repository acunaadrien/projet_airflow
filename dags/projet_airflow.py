import datetime;
from datetime import timedelta
import requests
import json
import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
#import pymongo

args = {
    'owner': 'Adrien',
    'start_date': airflow.utils.dates.days_ago(0),
}

dag = DAG(
  dag_id='projet_airflow',
  schedule_interval=timedelta(minutes = 1), 
  default_args=args,
)

startUrl = "https://financialmodelingprep.com/api/v3"
apiKey = "?apikey=ab9b8eeda9ce1d450638d2a57a5e8c3e"


def getRequest(route):
    request = startUrl + route
    r = requests.get(request + apiKey)
    return r.json()
    
def getAppleProfile():
    return getRequest("/profile/AAPL")

def getAppleRating():
    return getRequest("/rating/AAPL")

    
def get_Profile_Rating_Timestamp():
    data = {
        "profile": getAppleProfile(),
        "rating": getAppleRating(),
        "timestamp": datetime.datetime.now().timestamp()
        }
    with open('/tmp/profileRatingTimestampApple.json', 'w') as outfile:
        json.dump(data, outfile)


getProfileRatingTimestamp = PythonOperator(
    task_id='getProfileRatingTimestamp',
    python_callable=get_Profile_Rating_Timestamp,
    dag=dag,
)

getProfileRatingTimestamp